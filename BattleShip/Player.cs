﻿using System;
using System.Collections.Generic;
using BattleShip.Ships;

namespace BattleShip
{
    public class Player
    {
        public string Name { get; set; }
        public readonly GameBoard GameBoard;
        public readonly List<Ship> Ships = new List<Ship>();

        public Player(string name)
        {
            this.Name = name;
            this.GameBoard = new GameBoard();

            Ships.Add(new PatrolBoat());
            Ships.Add(new PatrolBoat());
            Ships.Add(new PatrolBoat());
            Ships.Add(new PatrolBoat());
            Ships.Add(new Submarine());
            Ships.Add(new Submarine());
            Ships.Add(new Submarine());
            Ships.Add(new Destroyer());
            Ships.Add(new Destroyer());
            Ships.Add(new Carrier());
        }
        
        public Panel GetPanelByCoordinate()
        {
            Console.WriteLine("\nEnter coordinate relatively to  'X' ");
            var row = CoordinateValidation.Validation(Console.ReadLine());
            Console.WriteLine("Enter coordinate relatively to  'Y' ");
            var column = CoordinateValidation.Validation(Console.ReadLine());

            return GameBoard.Panels.Find(x => x.Coordinate.Row == row && x.Coordinate.Column == column);
        }

        public void Shot(Panel panel)
        {
            if (panel.Type == OccupationType.Miss || panel.Type == OccupationType.Hit)
            {
                Console.WriteLine($"The shot, {panel.Coordinate.Row}:{panel.Coordinate.Column}, has been already done");
                return;
            }

            if (panel.Type == OccupationType.IsFree)
            {
                panel.Type = OccupationType.Miss;
                Console.WriteLine($"The shot, {panel.Coordinate.Row}:{panel.Coordinate.Column}, is failed");
            }
            else
            {
                panel.Type = OccupationType.Hit;
                panel.Ship.Shots++;
                var shotResult = (panel.Ship.IsDestroyed) ? "destroyed" : "hit";
                Console.WriteLine($"The shot, {panel.Coordinate.Row}:{panel.Coordinate.Column}, has {shotResult} the {panel.Ship.Name}");
            }
        }

        public void DrawBoard()
        {
            Console.WriteLine();
            foreach (var panel in this.GameBoard.Panels)
            {
                Console.Write((panel.ToString()));

                if (panel.Coordinate.Column == 10)
                    Console.WriteLine();
            }
        }
    }
}