﻿using System.Collections.Generic;

namespace BattleShip
{
    public class GameBoard
    {
        public List<Panel> Panels { get; }

        public GameBoard()
        {
            Panels = new List<Panel>();

            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    Panels.Add(new Panel(i,j));
                }
            }
        }
    }
}