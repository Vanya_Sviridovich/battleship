﻿namespace BattleShip
{
    public enum OccupationType
    {
        IsFree = '*',
        IsNotAvailableForShipGeneration,
        Carrier = 'C',
        Destroyer = 'D',
        Submarine = 'S',
        PatrolBoat = 'P',
        Hit = 'X',
        Miss = 'M'
    }
}