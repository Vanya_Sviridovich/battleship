﻿using System;
using System.Collections.Generic;
using BattleShip.Ships;

namespace BattleShip.BoardModel
{
    public class ShipGeneration
    {
        private readonly GameBoard _gameBoard;
        private readonly Random _random = new Random();
        
        public ShipGeneration(GameBoard gameBoard)
        {
            this._gameBoard = gameBoard;
        }

        public void Generate(Ship ship)
        {
            var isOpen = true;

            while (isOpen)
            {
                //list with all free panels
                var allEmptyPanels = GetEmptyPanels(_gameBoard.Panels);

                //choose one panel for start placing new ship
                var targetPanel = allEmptyPanels[_random.Next(allEmptyPanels.Count)];

                // direction of rotation
                var direction = _random.Next(1, 10) % 2 == 0;

                var affectedPanels = new List<Panel>();

                if (direction)
                {
                    //if rows are out of the board boundary
                    if (targetPanel.Coordinate.Row + ship.Size > 10)
                        continue;

                    for (int i = 0; i < ship.Size; i++)
                        affectedPanels.Add(_gameBoard.Panels.Find(x=> x.Coordinate.Row == targetPanel.Coordinate.Row + i && x.Coordinate.Column == targetPanel.Coordinate.Column));
                }
                else
                {
                    //if columns are out of the board boundary
                    if (targetPanel.Coordinate.Column + ship.Size > 10)
                        continue;

                    for (int i = 0; i < ship.Size; i++)
                        affectedPanels.Add(_gameBoard.Panels.Find(x => x.Coordinate.Row == targetPanel.Coordinate.Row && x.Coordinate.Column == targetPanel.Coordinate.Column + i));
                }

                //??????????????????????????
                //var isOccupied = affectedPanels.All(x => x.Type != OccupationType.IsFree);

                var isOccupied = false;

                //check all panels for being free
                foreach (var affectedPanel in affectedPanels)
                {
                    if (affectedPanel.Type != OccupationType.IsFree)
                        isOccupied = true;
                }

                if (isOccupied)
                    continue;
                

                foreach (var panel in affectedPanels)
                {
                    panel.Type = ship.Type;
                    panel.Ship = ship;

                    ChangeStatus(panel.Coordinate.Row + 1, panel.Coordinate.Column);
                    ChangeStatus(panel.Coordinate.Row - 1, panel.Coordinate.Column);
                    ChangeStatus(panel.Coordinate.Row, panel.Coordinate.Column + 1);
                    ChangeStatus(panel.Coordinate.Row, panel.Coordinate.Column - 1);

                    ChangeStatus(panel.Coordinate.Row + 1, panel.Coordinate.Column + 1);
                    ChangeStatus(panel.Coordinate.Row + 1, panel.Coordinate.Column - 1);
                    ChangeStatus(panel.Coordinate.Row - 1, panel.Coordinate.Column + 1);
                    ChangeStatus(panel.Coordinate.Row - 1, panel.Coordinate.Column - 1);
                }
                isOpen = false;
            }
        }

        private void ChangeStatus(int x, int y)
        {
            foreach (var panel in _gameBoard.Panels)
            {
                if (x > 0 && x <= Math.Sqrt(_gameBoard.Panels.Count) && y > 0 &&
                    y <= Math.Sqrt(_gameBoard.Panels.Count))
                {
                    if (panel.Coordinate.Row == x && panel.Coordinate.Column == y && panel.Type == OccupationType.IsFree)
                        panel.Type = OccupationType.IsNotAvailableForShipGeneration;
                }
            }
        }
        private List<Panel> GetEmptyPanels(List<Panel> panels)
        {
            List<Panel> emptyPanels = new List<Panel>();

            foreach (var panel in panels)
            {
                if (panel.Type == OccupationType.IsFree)
                    emptyPanels.Add(panel);
            }

            return emptyPanels;
        }
    }
}