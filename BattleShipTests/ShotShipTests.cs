using NUnit.Framework;
using BattleShip;

namespace BattleShipTests
{
    [TestFixture]
    public class Tests
    {
        private Game _game;
        private Player _player;

        [SetUp]
        public void Setup()
        {
            _game = new Game();
            _player = _game.Player;
            _game.StartShipGeneration(_player.Ships);
        }
        [Test]
        public void Shot_TheShotIsMissed()
        {
            var targetPanel = _player.GameBoard.Panels.Find(x => x.Type == OccupationType.IsFree);

            _player.Shot(targetPanel);

            Assert.AreEqual(targetPanel.Type, OccupationType.Miss);
        }

        [Test]
        public void Shot_ShipIsSunk()
        {
            var targetPanel = _player.GameBoard.Panels.Find(x => x.Type == OccupationType.PatrolBoat);

            _player.Shot(targetPanel);

            Assert.IsTrue(targetPanel.Ship.IsDestroyed);
        }

        [Test]
        public void Shot_ShipHit()
        {
            var targetPanel = _player.GameBoard.Panels.Find(x => x.Type == OccupationType.Carrier);

            _player.Shot(targetPanel);

            Assert.IsFalse(targetPanel.Ship.IsDestroyed);
        }
    }
}