﻿using System;

namespace BattleShip
{
    public class CoordinateValidation
    {
        public static int Validation(string str)
        {
            while (true)
            {
                if (Int32.TryParse(str, out var number) && number >= 1 && number <= 10)
                    return number;

                Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop - 1);
                Console.Write(new String(' ', Console.BufferWidth));
                Console.SetCursorPosition(0, Console.CursorTop);
                str = Console.ReadLine();
            }
        }
    }
}