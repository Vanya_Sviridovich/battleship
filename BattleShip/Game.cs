﻿using System;
using System.Collections.Generic;
using System.Linq;
using BattleShip.BoardModel;
using BattleShip.Ships;

namespace BattleShip
{
    public class Game
    {
        public readonly Player Player;
        public readonly ShipGeneration ShipGeneration;
        public readonly ShipStatistics ShipStatistics;

        public Game()
        {
            this.Player = new Player("Mr.Smith");
            this.ShipGeneration = new ShipGeneration(this.Player.GameBoard);
            this.ShipStatistics = new ShipStatistics(Player.Ships);
        }

        public void Play()
        {
            StartShipGeneration(Player.Ships);

            while (!Player.Ships.All(x => x.IsDestroyed))
            {
                Console.WriteLine("Get Json state of all ships by pressing [SpaceBar]");

                if (Console.ReadKey().Key == ConsoleKey.Spacebar)
                    ShipStatistics.DisplayShipState();
                
                var targetPanel = Player.GetPanelByCoordinate();
                Player.Shot(targetPanel);
            }
            Console.WriteLine("Game Over");
        }

        public void StartShipGeneration(List<Ship> ships)
        {
            //random generation
            foreach (var ship in ships)
                ShipGeneration.Generate(ship);

            //change status after generation IsNotAvailableForShipGeneration to IsFree
            foreach (var panel in this.Player.GameBoard.Panels)
            {
                if (panel.Type == OccupationType.IsNotAvailableForShipGeneration)
                    panel.Type = OccupationType.IsFree;
            }
        }
    }
}