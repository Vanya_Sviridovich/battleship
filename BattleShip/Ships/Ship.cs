﻿using System.Text.Json.Serialization;

namespace BattleShip.Ships
{
    public abstract class Ship
    {
        public string Name { get; set; }
        public int Size { get; set; }
        public int Shots { get; set; }
        [JsonIgnore]
        public OccupationType Type { get; set; }

        public bool IsDestroyed => this.Shots >= this.Size;

    }
}