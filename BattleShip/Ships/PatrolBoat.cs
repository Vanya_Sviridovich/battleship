﻿namespace BattleShip.Ships
{
    public class PatrolBoat : Ship
    {
        public PatrolBoat()
        {
            this.Name = "PatrolBoat";
            this.Size = 1;
            this.Type = OccupationType.PatrolBoat;
        }
    }
}