﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using BattleShip.Ships;

namespace BattleShip
{
    public class ShipStatistics
    {
        public readonly Dictionary<string, List<Ship>> ShipState = new Dictionary<string, List<Ship>>();
        public readonly List<Ship> UserShip;

        public ShipStatistics(List<Ship> ships)
        {
            UserShip = ships;
            ShipState.Add("IsFull", new List<Ship>());
            ShipState.Add("IsHit", new List<Ship>());
            ShipState.Add("IsSunk", new List<Ship>());
        }

        private void SortShipByItsState()
        {
            foreach (var ship in UserShip)
            {
                if (ship.Shots == 0)
                    ShipState["IsFull"].Add(ship);

                else if(ship.Shots > 0 && !ship.IsDestroyed)
                    ShipState["IsHit"].Add(ship);
                
                else if (ship.IsDestroyed)
                    ShipState["IsSunk"].Add(ship);
            }
        }

        public void DisplayShipState()
        {
            SortShipByItsState();

            foreach (var keyValuePair in ShipState)
            {
                Console.WriteLine("\n"+keyValuePair.Key);

                if (ShipState[keyValuePair.Key].Count == 0)
                {
                    Console.Write("0");
                    continue;
                }

                foreach (var ship in ShipState[keyValuePair.Key])
                {
                    string json = JsonSerializer.Serialize<Ship>(ship);
                    Console.WriteLine(json);
                }
                ShipState[keyValuePair.Key].Clear();
            }
        }
    }
}