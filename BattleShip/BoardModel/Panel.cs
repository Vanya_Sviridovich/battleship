﻿using System.Dynamic;
using BattleShip.Ships;

namespace BattleShip
{
    public class Panel
    {
        public Coordinate Coordinate { get; set; }
        public OccupationType Type { get; set; }

        public Ship Ship { get; set; }
        
        public Panel(int row, int column)
        {
            this.Coordinate = new Coordinate(row, column);
            this.Type = OccupationType.IsFree;
        }

        public override string ToString()
        {
            return ((char) Type) + " ";
            //return $"{Coordinate.Row}{Coordinate.Column} ";
        }
    }
}